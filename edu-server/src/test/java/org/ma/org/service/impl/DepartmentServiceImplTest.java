package org.ma.org.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ma.EduServerApplication;
import org.ma.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = EduServerApplication.class)
@RunWith(SpringRunner.class)
public class DepartmentServiceImplTest {

    @Autowired
    private IDepartmentService service;

    @Test
    public void insert() {
    }

    @Test
    public void delete() {
        service.delete(23);
    }

    @Test
    public void update() {
    }

    @Test
    public void selectAll() {
    }

    @Test
    public void selectById() {
    }
}