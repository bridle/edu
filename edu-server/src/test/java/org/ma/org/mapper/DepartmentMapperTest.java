package org.ma.org.mapper;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.ma.EduServerApplication;
import org.ma.org.domain.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = EduServerApplication.class)
@RunWith(SpringRunner.class)
public class DepartmentMapperTest {
    @Autowired
    private DepartmentMapper mapper;

    @Test
    public void insert() {
        Department department = mapper.selectById(1);
        mapper.insert(department);
    }
    @Test
    public void delete() {
        mapper.delete(24);
    }
    @Test
    public void update() {
        Department department = mapper.selectById(24);
        department.setName("wahha");
        mapper.update(department);
    }
    @Test
    public void selectAll() {
        mapper.selectAll().forEach(System.out::println);
    }
    @Test
    public void selectById() {
        System.out.println(mapper.selectById(5));
    }
}