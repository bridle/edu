package org.ma.user.domain;

import java.util.Date;
import org.ma.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类：
 */
@Data
@AllArgsConstructor  //全参构造
@NoArgsConstructor //无参构造
public class Wxuser extends BaseDomain{

    @ApiModelProperty("表的主键")
    private Long id;
    @ApiModelProperty("是微信返给三方的唯一标识")
    private String openid;
    @ApiModelProperty("微信昵称")
    private String nickname;
    @ApiModelProperty("性别")
    private Integer sex;
    @ApiModelProperty("地址")
    private String address;
    @ApiModelProperty("头像")
    private String headimgurl;
    @ApiModelProperty("微信生态的id")
    private String unionid;
    @ApiModelProperty("关联user表的id")
    private Long userId;
}