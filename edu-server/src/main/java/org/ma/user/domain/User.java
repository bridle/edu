package org.ma.user.domain;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import org.ma.basic.domain.BaseDomain;

/**
 * 实体类：
 */
@Data
@AllArgsConstructor  //全参构造
@NoArgsConstructor //无参构造
public class User extends BaseDomain {

    private Long id;
    private String username;
    private String phone;
    private String email;
    @ApiModelProperty("盐值")
    private String salt;
    @ApiModelProperty("密码，md5加密加盐")
    private String password;
    @ApiModelProperty("员工状态 - 1启用，0禁用")
    private Integer state;
    private Integer age;
    private Date createtime;
    private String headImg;
    private Long logininfoId;
}
