package org.ma.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import org.ma.basic.domain.BaseDomain;

/**
 * 实体类：
 */
@Data
@AllArgsConstructor  //全参构造
@NoArgsConstructor //无参构造
public class Logininfo extends BaseDomain {

    private Long id;
    private String username;
    private String phone;
    private String email;
    private String salt;
    private String password;
    @ApiModelProperty("类型 - 0管理员，1用户")
    private Integer type;
    @ApiModelProperty("启用状态：true可用，false禁用")
    private Boolean disable;
}
