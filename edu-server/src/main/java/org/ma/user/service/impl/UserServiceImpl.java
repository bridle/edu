package org.ma.user.service.impl;

import org.ma.basic.service.impl.BaseServiceImpl;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.StrUtils;
import org.ma.user.domain.Logininfo;
import org.ma.user.domain.User;
import org.ma.user.mapper.LogininfoMapper;
import org.ma.user.mapper.UserMapper;
import org.ma.user.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * 业务实现类：
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private LogininfoMapper logininfoMapper;

    @Autowired
    private UserMapper userMapper;

    // 获取手机验证码的方法
    @Override
    public AjaxResult register(Map<String, String> map) {

        // 1. 空值判断 手机号 短信验证码 密码
        String phone = map.get("mobile");
        String password = map.get("password");
        String smsCode = map.get("smsCode");
        String regChannel = map.get("regChannel");
        if (Objects.isNull(password) || Objects.isNull(phone) || Objects.isNull(smsCode)) {
            return AjaxResult.me().setSuccess(false).setMessage("参数不正确");
        }
        // 2. 验证手机验证码 是否匹配
        Map entries = redisTemplate.opsForHash().entries("reg_" + phone);
        if (Objects.isNull(entries)) {
            return AjaxResult.me().setSuccess(false).setMessage("短信验证码失效。");
        }
        String smsCode1 = (String) entries.get("smsCode");
        if (!smsCode.equals(smsCode1)) {
            return AjaxResult.me().setSuccess(false).setMessage("短信验证码错误。");
        }
        // 判断用户是否存在
        // 3. 输入入库
        User user = new User();
        user.setPassword(password);
        user.setPhone(phone);
        insert(user);
        return AjaxResult.me();
    }

    public Logininfo user2LoginInfo(User user, String regChannel) {
        Logininfo loginInfo = new Logininfo();
        BeanUtils.copyProperties(user, loginInfo);
        loginInfo.setId(null);
        loginInfo.setDisable(true);
        loginInfo.setType(Integer.parseInt(regChannel));
        return loginInfo;
    }


    @Transactional
    @Override
    public void insert(User user) {
        //1 生成盐
        String salt = StrUtils.getComplexRandomString(32);
        user.setSalt(salt);
        // MD5加密
        String password = DigestUtils.md5DigestAsHex((salt + user.getPassword()).getBytes());
        user.setPassword(password);
        user.setState(1);
        user.setCreatetime(new Date());
        // 同步logininfo
        Logininfo logininfo = new Logininfo();
        BeanUtils.copyProperties(user, logininfo);
        logininfo.setType(1);
        logininfo.setDisable(true);
        logininfoMapper.insert(logininfo);
        user.setLogininfoId(logininfo.getId());
        userMapper.insert(user);

    }

    @Transactional
    @Override
    public void delete(Serializable id ) {
//        logininfoMapper.delete(user.getLogininfoId());
        userMapper.delete(id);
    }

    @Transactional
    @Override
    public void update(User user) {
        User user1 = userMapper.selectById(user.getId());

        Logininfo logininfo = new Logininfo();
        BeanUtils.copyProperties(user1, logininfo);
        logininfo.setType(1);
        logininfo.setDisable(true);
        logininfo.setId(user.getLogininfoId());
        logininfoMapper.update(logininfo);
        userMapper.update(user);
    }

}
