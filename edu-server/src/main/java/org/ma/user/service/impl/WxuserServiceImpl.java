package org.ma.user.service.impl;

import org.ma.user.domain.Wxuser;
import org.ma.user.service.IWxuserService;
import org.ma.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 业务实现类：
 */
@Service
public class WxuserServiceImpl extends BaseServiceImpl<Wxuser> implements IWxuserService {

}
