package org.ma.user.service;

import org.ma.user.domain.Wxuser;
import org.ma.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ma
 * @since 2024-08-29
 */
public interface IWxuserService extends IBaseService<Wxuser> {

}
