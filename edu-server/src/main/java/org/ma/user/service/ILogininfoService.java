package org.ma.user.service;

import org.ma.basic.service.IBaseService;
import org.ma.basic.util.AjaxResult;
import org.ma.user.domain.Logininfo;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiong
 * @since 2024-08-26
 */
public interface ILogininfoService extends IBaseService<Logininfo> {
    AjaxResult login(Map<String, String> map);
    AjaxResult loginWechat(String code);

    AjaxResult binder(Map<String, String> map);
}
