package org.ma.user.service;


import org.ma.basic.service.IBaseService;
import org.ma.basic.util.AjaxResult;
import org.ma.user.domain.User;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiong
 * @since 2024-08-26
 */
public interface IUserService extends IBaseService<User> {

    AjaxResult register(Map<String,String> map);
}
