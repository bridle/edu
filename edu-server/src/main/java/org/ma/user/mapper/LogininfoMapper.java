package org.ma.user.mapper;


import org.ma.basic.mapper.BaseMapper;
import org.ma.user.domain.Logininfo;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiong
 * @since 2024-08-26
 */
public interface LogininfoMapper extends BaseMapper<Logininfo> {

    Logininfo queryByPhone(String phone);
    Logininfo findLoginInfoByAccountAndType(Map<String, String> map);
}

