package org.ma.user.mapper;


import org.ma.basic.mapper.BaseMapper;
import org.ma.user.domain.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiong
 * @since 2024-08-26
 */
public interface UserMapper extends BaseMapper<User> {

}
