package org.ma.user.mapper;

import org.ma.user.domain.User;
import org.ma.user.domain.Wxuser;
import org.ma.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ma
 * @since 2024-08-29
 */
public interface WxuserMapper extends BaseMapper<Wxuser> {
    Wxuser findByOpenId(String openid);

    User findByWxUserId(Long userId);
}
