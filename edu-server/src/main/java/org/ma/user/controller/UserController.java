package org.ma.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.PageInfo;
import org.ma.user.domain.User;
import org.ma.user.query.UserQuery;
import org.ma.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/user")
@Api(tags = "UserController")
public class UserController {

    @Autowired
    public IUserService userService;

    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    @ApiOperation(value = "查询所有")
    public AjaxResult loadAll(){
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        List<User> allList = userService.selectAll();
        ajaxResult.setResultObj(allList);
        return ajaxResult;
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询单个")
    public AjaxResult getOne(@PathVariable("id")Long id) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        User one=userService.selectById(id);
        ajaxResult.setResultObj(one);
        return ajaxResult;
    }
    /**
     * 接口：添加或修改
     * @param user  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    @ApiOperation(value = "添加或修改")
    public AjaxResult saveOne(@RequestBody User user){
        if( user.getId()==null){
            userService.insert(user);
        }else{
            userService.update(user);
	    }
        return AjaxResult.me();
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    @ApiOperation(value = "删除单个")
    public AjaxResult deleteOne(@PathVariable("id") Long id){
        userService.delete(id);
        return AjaxResult.me();
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    @ApiOperation(value = "批量删除")
    public AjaxResult patchDel(@RequestBody List<Long> ids){
        userService.batchDelete(ids);
        return new AjaxResult();
    }

   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    @ApiOperation(value = "分页查询或高级查询")
    public AjaxResult queryPage(@RequestBody UserQuery query) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        PageInfo<User> pageList=userService.queryPage(query);
        ajaxResult.setResultObj(pageList);
        return ajaxResult;
    }


    /**
     * 用户注册
     * @param map
     * @return
     */
    @PostMapping("/register")
    @ApiOperation(value = "用户注册")
    public AjaxResult register(@RequestBody Map<String,String> map) {
        try {

            return userService.register(map);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("注册失败");
        }
    }
}
