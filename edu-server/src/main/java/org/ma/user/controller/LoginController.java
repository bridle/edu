package org.ma.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.user.service.ILogininfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/login")
@Api(tags = "loginController登录相关")
public class LoginController {

    @Autowired
    public ILogininfoService logininfoService;


    @PostMapping("/login")
    @ApiOperation(value = "登录")
    public AjaxResult login(@RequestBody Map<String,String> map) {
        try {
            return  logininfoService.login(map);
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("登录失败");
        }
    }
    @PostMapping("/login/wechat")
    @ApiOperation(value = "微信登录")
    public AjaxResult loginWechat(@RequestBody Map<String,String> map) {
        try {
            return  logininfoService.loginWechat(map.get("code"));
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("微信登录失败");
        }
    }
    @PostMapping("/wechat/binder")
    @ApiOperation(value = "微信绑定")
    public AjaxResult binder(@RequestBody Map<String,String> map) {
        try {
            return  logininfoService.binder(map);
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("微信绑定失败");
        }
    }
}
