package org.ma.user.controller;

import org.ma.user.service.IWxuserService;
import org.ma.user.domain.Wxuser;
import org.ma.user.query.WxuserQuery;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/wxuser")
@Api(tags = "WxuserController")
public class WxuserController {

    @Autowired
    public IWxuserService wxuserService;

    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    @ApiOperation(value = "查询所有")
    public AjaxResult loadAll(){
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        List<Wxuser> allList = wxuserService.selectAll();
        ajaxResult.setResultObj(allList);
        return ajaxResult;
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询单个")
    public AjaxResult getOne(@PathVariable("id")Long id) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        Wxuser one=wxuserService.selectById(id);
        ajaxResult.setResultObj(one);
        return ajaxResult;
    }
    /**
     * 接口：添加或修改
     * @param wxuser  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    @ApiOperation(value = "添加或修改")
    public AjaxResult saveOne(@RequestBody Wxuser wxuser){
        if( wxuser.getId()==null){
            wxuserService.insert(wxuser);
        }else{
            wxuserService.update(wxuser);
	    }
        return AjaxResult.me();
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    @ApiOperation(value = "删除单个")
    public AjaxResult deleteOne(@PathVariable("id") Long id){
        wxuserService.delete(id);
        return AjaxResult.me();
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    @ApiOperation(value = "批量删除")
    public AjaxResult patchDel(@RequestBody List<Long> ids){
        wxuserService.batchDelete(ids);
        return new AjaxResult();
    }

   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    @ApiOperation(value = "分页查询或高级查询")
    public AjaxResult queryPage(@RequestBody WxuserQuery query) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        PageInfo<Wxuser> pageList=wxuserService.queryPage(query);
        ajaxResult.setResultObj(pageList);
        return ajaxResult;
    }
}
