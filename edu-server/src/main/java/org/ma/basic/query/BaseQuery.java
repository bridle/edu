package org.ma.basic.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseQuery {
    // 查询条件
    private String keyword;
    // 每页条数
    private Integer pageSize;
    // 当前页
    private Integer currentPage;

    public Integer getStart(){
        return (currentPage-1)*pageSize;
    }
}
