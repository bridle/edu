package org.ma.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageInfo<T> {
     // 总数
    private Long total =0L;
    // 返回的List 数据 默认null
    private List<T> list = new ArrayList<>();
}
