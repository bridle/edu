package org.ma.basic.util;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;

public class SendSMSUtils {
    public static void senSMS(String phone,String code)  {
        try {
            String randomString = StrUtils.getRandomString(6);
            System.out.println(randomString);
            HttpClient client = new HttpClient();
            PostMethod post = new PostMethod("https://utf8api.smschinese.cn/");
            post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");//在头文件中设置转码
            NameValuePair[] data ={ new NameValuePair("Uid", "水木恬平"),new NameValuePair("Key", "510B5BF4BB38C752B12731A375E7213F"),new NameValuePair("smsMob",phone),new NameValuePair("smsText","验证码:"+code)};
            post.setRequestBody(data);

            client.executeMethod(post);
            Header[] headers = post.getResponseHeaders();
            int statusCode = post.getStatusCode();
            System.out.println("statusCode:"+statusCode); //HTTP状态码
            for(Header h : headers){
                System.out.println(h.toString());
            }
            String result = new String(post.getResponseBodyAsString().getBytes("utf-8"));
            System.out.println(result);  //打印返回消息状态

            post.releaseConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
