package org.ma.basic.controller;

import io.swagger.annotations.Api;
import org.ma.basic.service.IVerifyCodeService;
import org.ma.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/common/verifycode")
@Api(tags = "验证码的Controller")
public class VerifyCodeController {
    @Autowired
    private IVerifyCodeService verifyCodeService;

    @GetMapping("/imageCode/{ImageCodeKey}")
    public AjaxResult getImageCode(@PathVariable("ImageCodeKey") String ImageCodeKey){
        try {
            // 获取验证码
            String base64ImageCode = verifyCodeService.getImageCode(ImageCodeKey);
            // 返回数据
            return  AjaxResult.me().setResultObj(base64ImageCode);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("图形验证码获取失败~请刷新");
        }
    }

    @PostMapping("/sendSmsCode")
    public AjaxResult sendSmsCode(@RequestBody Map<String,String> map){
        try {
            // 获取手机验证码

            // 返回数据
            return  verifyCodeService.sendSmsCode(map);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("获取手机验证码失败。。");
        }
    }

    @PostMapping("/smsbind")
    public AjaxResult smsbind(@RequestBody Map<String,String> map){
        try {
            // 获取手机验证码

            // 返回数据
            return  verifyCodeService.smsbind(map);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("获取手机验证码失败。。");
        }
    }
}
