package org.ma.basic.controller;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;

import java.io.*;

import io.swagger.annotations.Api;
import org.ma.basic.domain.AlicloudOSSProperties;
import org.ma.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/oss")
@Api("OSSController")
public class OSSController {
    @Autowired
    private AlicloudOSSProperties alicloudOSSProperties;

    @GetMapping("/sign")
    protected AjaxResult doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Endpoint以华东1（杭州）为例，其他Region请按实际情况填写。
        String endpoint = alicloudOSSProperties.getEndpoint();
        // 填写Bucket名称，例如examplebucket。
        String bucket = alicloudOSSProperties.getBucketName();
        // 填写Host名称，格式为https://bucketname.endpoint。
        String host = "https://"+alicloudOSSProperties.getBucketName()+"."+alicloudOSSProperties.getEndpoint();
        // 设置上传到OSS文件的前缀，可置空此项。置空后，文件将上传至Bucket的根目录下。
        String dir = "images";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, alicloudOSSProperties.getAccessKey(),alicloudOSSProperties.getSecretKey());
        try {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            // PostObject请求最大可支持的文件大小为5 GB，即CONTENT_LENGTH_RANGE为5*1024*1024*1024。
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = ossClient.calculatePostSignature(postPolicy);

            Map<String, String> respMap = new LinkedHashMap<String, String>();
            respMap.put("accessid", alicloudOSSProperties.getAccessKey());
            respMap.put("policy", encodedPolicy);
            respMap.put("signature", postSignature);
            respMap.put("dir", dir);
            respMap.put("host", host);
            respMap.put("expire", String.valueOf(expireEndTime / 1000));
            // respMap.put("expire", formatISO8601Date(expiration));

            return AjaxResult.me().setResultObj(respMap);

        } catch (Exception e) {
            // Assert.fail(e.getMessage());
            System.out.println(e.getMessage());
            return AjaxResult.me().setSuccess(false).setMessage("OSS失败");
        } finally {
            ossClient.shutdown();
        }
    }

}
