package org.ma.basic.mapper;

import org.ma.basic.query.BaseQuery;

import java.io.Serializable;
import java.util.List;

public interface BaseMapper<T> {
    /** 新增 */
    void insert(T t);
    /** 删除 */
    void delete(Serializable id);
    /** 修改 */
    void update(T t);
    /** 查询全部 */
    List<T> selectAll();
    /** 查询指定部门 */
    T selectById(Serializable id);

    Long queryTotal(BaseQuery baseQuery);

    List<T> queryPage(BaseQuery baseQuery);

    void batchDelete(List<Long> ids);
}
