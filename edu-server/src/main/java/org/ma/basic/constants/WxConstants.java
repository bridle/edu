package org.ma.basic.constants;

public class WxConstants {
    // 微信扫码是需要的 三方应用id
    public static final String APPID = "wx03bd86056bd3fc0e";
    // 微信扫码的三方 密钥
    public static final String SECRET = "f52fb26ebd494fe6f44eb4da50bd22da";
    // 请求token的路径                         https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
    public static final String GET_ACK_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
    // 第三次请求的路径 - 获取微信用户信息的路径
    public static final String GET_USER_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID";

}