package org.ma.basic.service;


import org.ma.basic.util.AjaxResult;

import java.io.IOException;
import java.util.Map;

public interface IVerifyCodeService {
    String getImageCode(String imageCodeKey) throws IOException;

    AjaxResult sendSmsCode(Map<String, String> map);

    AjaxResult smsbind(Map<String, String> map);

}
