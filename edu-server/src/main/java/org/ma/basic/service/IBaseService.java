package org.ma.basic.service;

import org.ma.basic.query.BaseQuery;
import org.ma.basic.util.PageInfo;

import java.io.Serializable;
import java.util.List;

public interface IBaseService<T> {
    /** 新增 */
    void insert(T t);
    /** 删除 */
    void delete(Serializable id);
    /** 修改 */
    void update(T t);
    /** 查询全部 */
    List<T> selectAll();
    /** 查询指定部门 */
    T selectById(Serializable id);

    void batchDelete(List<Long> ids);

    PageInfo<T> queryPage(BaseQuery baseQuery);


}
