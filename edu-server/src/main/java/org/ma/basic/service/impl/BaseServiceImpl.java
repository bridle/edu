package org.ma.basic.service.impl;
import org.ma.basic.mapper.BaseMapper;
import org.ma.basic.query.BaseQuery;
import org.ma.basic.service.IBaseService;
import org.ma.basic.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;


public class BaseServiceImpl<T> implements IBaseService<T> {
    @Autowired
    private BaseMapper<T> baseMapper;


    @Transactional
    @Override
    public void insert(T t) {
        baseMapper.insert(t);

    }
    @Transactional
    @Override
    public void delete(Serializable id) {

        baseMapper.delete(id);
    }
    @Transactional
    @Override
    public void update(T t) {

        baseMapper.update(t);
    }

    @Override
    public List<T> selectAll() {
        return baseMapper.selectAll();
    }

    @Override
    public T selectById(Serializable id) {
        return baseMapper.selectById(id);
    }
    @Transactional
    @Override
    public void batchDelete(List<Long> ids) {
        baseMapper.batchDelete(ids);


    }

    @Override
    public PageInfo<T> queryPage(BaseQuery baseQuery) {
        // 高级查询和分页 只需要查 分页的数据 list = 10条 就OK了???  还要查total
        PageInfo<T> pageInfo = new PageInfo<>();
        // 先查 total
        Long total = baseMapper.queryTotal(baseQuery);
        if (total != 0){
            List<T> list = baseMapper.queryPage(baseQuery);
            pageInfo.setList(list);
            pageInfo.setTotal(total);
        }
        // 在查 list
        return pageInfo;
    }

   
}
