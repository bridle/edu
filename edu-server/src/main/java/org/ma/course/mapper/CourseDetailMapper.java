package org.ma.course.mapper;


import org.ma.basic.mapper.BaseMapper;
import org.ma.course.domain.CourseDetail;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiong
 * @since 2024-08-28
 */
public interface CourseDetailMapper extends BaseMapper<CourseDetail> {

}
