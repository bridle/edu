package org.ma.course.mapper;


import org.ma.basic.mapper.BaseMapper;
import org.ma.course.domain.Teacher;

/**
 * <p>
 * 老师表 Mapper 接口
 * </p>
 *
 * @author xiong
 * @since 2024-08-23
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
