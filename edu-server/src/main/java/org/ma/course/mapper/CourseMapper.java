package org.ma.course.mapper;


import org.apache.ibatis.annotations.Param;
import org.ma.basic.mapper.BaseMapper;
import org.ma.course.domain.Course;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiong
 * @since 2024-08-23
 */
public interface CourseMapper extends BaseMapper<Course> {
    List<Course> selectByIds(@Param("ids") List<Long> ids, @Param("status") Integer status);

    void onsaleOrOffSale(@Param("ids") List<Course> list, @Param("status") Integer status);
}
