package org.ma.course.mapper;


import org.ma.basic.mapper.BaseMapper;
import org.ma.course.domain.CourseType;

public interface CourseTypeMapper extends BaseMapper<CourseType> {

}


