package org.ma.course.doc;

import lombok.Data;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@Document(indexName = "rh-course")
@Data
public class CourseDoc {
    @Id
    private Long id;
    @Field(type = FieldType.Text)
    private String name;
    @Field(type = FieldType.Long)
    private Long courseTypeId;
    @Field(type = FieldType.Text)
    private String gradeName;
    @Field(type = FieldType.Long)
    private Long gradeId;
    @Field(type = FieldType.Integer)
    private Integer status;
    @Field(type = FieldType.Date)
    private Date startTime;
    @Field(type = FieldType.Date)
    private Date endTime;
    @Field(type = FieldType.Keyword)
    private String pic;
    @Field(type = FieldType.Date)
    private Date onlineTime;
    @Field(type = FieldType.Integer)
    private Integer chapterCount;
    @Field(type = FieldType.Text)
    private String teacherNames;
    @Field(type = FieldType.Text)
    private String intro;
}
