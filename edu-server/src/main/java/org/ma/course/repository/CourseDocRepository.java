package org.ma.course.repository;

import org.ma.course.doc.CourseDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseDocRepository extends ElasticsearchRepository<CourseDoc,Long> {
}
