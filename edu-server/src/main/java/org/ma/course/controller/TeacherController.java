package org.ma.course.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.PageInfo;
import org.ma.course.domain.Teacher;
import org.ma.course.query.TeacherQuery;
import org.ma.course.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/teacher")
@Api(tags = "TeacherController")
public class TeacherController {

    @Autowired
    public ITeacherService teacherService;

    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    @ApiOperation(value = "查询所有")
    public AjaxResult loadAll(){
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        List<Teacher> allList = teacherService.selectAll();
        ajaxResult.setResultObj(allList);
        return ajaxResult;
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询单个")
    public AjaxResult getOne(@PathVariable("id")Long id) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        Teacher one=teacherService.selectById(id);
        ajaxResult.setResultObj(one);
        return ajaxResult;
    }
    /**
     * 接口：添加或修改
     * @param teacher  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    @ApiOperation(value = "添加或修改")
    public AjaxResult saveOne(@RequestBody Teacher teacher){
        if( teacher.getId()==null){
            teacherService.insert(teacher);
        }else{
            teacherService.update(teacher);
	    }
        return AjaxResult.me();
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    @ApiOperation(value = "删除单个")
    public AjaxResult deleteOne(@PathVariable("id") Long id){
        teacherService.delete(id);
        return AjaxResult.me();
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    @ApiOperation(value = "批量删除")
    public AjaxResult patchDel(@RequestBody List<Long> ids){
        teacherService.batchDelete(ids);
        return new AjaxResult();
    }

   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    @ApiOperation(value = "分页查询或高级查询")
    public AjaxResult queryPage(@RequestBody TeacherQuery query) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        PageInfo<Teacher> pageList=teacherService.queryPage(query);
        ajaxResult.setResultObj(pageList);
        return ajaxResult;
    }
}
