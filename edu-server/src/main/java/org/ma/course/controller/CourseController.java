package org.ma.course.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.PageInfo;
import org.ma.course.domain.Course;
import org.ma.course.query.CourseQuery;
import org.ma.course.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/course")
@Api(tags = "CourseController")
public class CourseController {

    @Autowired
    public ICourseService courseService;

    /**
     * 接口：查询所有
     * @return
     */
    @GetMapping
    @ApiOperation(value = "查询所有")
    public AjaxResult loadAll(){
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        List<Course> allList = courseService.selectAll();
        ajaxResult.setResultObj(allList);
        return ajaxResult;
    }

    /**
     * 接口：查询单个对象
     * @param id
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询单个")
    public AjaxResult getOne(@PathVariable("id")Long id) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        Course one=courseService.selectById(id);
        ajaxResult.setResultObj(one);
        return ajaxResult;
    }
    /**
     * 接口：添加或修改
     * @param course  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    @ApiOperation(value = "添加或修改")
    public AjaxResult saveOne(@RequestBody Course course){
        if( course.getId()==null){
            courseService.insert(course);
        }else{
            courseService.update(course);
        }
        return AjaxResult.me();
    }

    /**
     * 接口：删除
     * @param id
     * @return AjaxResult 响应给前端
     */
    @DeleteMapping(value="/{id}")
    @ApiOperation(value = "删除单个")
    public AjaxResult deleteOne(@PathVariable("id") Long id){
        courseService.delete(id);
        return AjaxResult.me();
    }

    /**
     * 接口：批量删除
     * @param ids
     * @return AjaxResult 响应给前端
     */
    @PatchMapping
    @ApiOperation(value = "批量删除")
    public AjaxResult patchDel(@RequestBody List<Long> ids){
        courseService.batchDelete(ids);
        return new AjaxResult();
    }

    /**
     * 接口：分页查询或高级查询
     * @param query 查询对象
     * @return PageList 分页对象
     */
    @PostMapping
    @ApiOperation(value = "分页查询或高级查询")
    public AjaxResult queryPage(@RequestBody CourseQuery query) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        PageInfo<Course> pageList=courseService.queryPage(query);
        ajaxResult.setResultObj(pageList);
        return ajaxResult;
    }


    @PostMapping("/upload")
    @ApiOperation(value = "分页查询或高级查询")
    public AjaxResult upload(MultipartFile file) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        AjaxResult upload = courseService.upload(file);
        return upload;
    }


    @PostMapping("/onsale")
    @ApiOperation(value = "批量上架")
    public AjaxResult onsale(@RequestBody List<Long> ids) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        AjaxResult upload = courseService.onsale(ids);
        return upload;
    }

    @PostMapping("/offsale")
    @ApiOperation(value = "批量下架")
    public AjaxResult offsale(@RequestBody List<Long> ids) {
        //返回值,默认就是成功
        AjaxResult ajaxResult=AjaxResult.me();
        AjaxResult upload = courseService.offsale(ids);
        return upload;
    }
}
