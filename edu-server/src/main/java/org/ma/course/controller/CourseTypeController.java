package org.ma.course.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.PageInfo;
import org.ma.course.domain.CourseType;
import org.ma.course.query.CourseTypeQuery;
import org.ma.course.service.ICourseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/courseType")
@Api(tags = "课程类型管理")
public class CourseTypeController {
    @Autowired
    private ICourseTypeService courseTypeService;
    // 增/ 改 put 和 post请求 拿值是从 body里面取的
    @PutMapping
    @ApiOperation(value = "新增/修改")
    public AjaxResult save(@RequestBody CourseType courseType){
        try {
           if (Objects.isNull(courseType.getId())){
               courseType.setCreateTime(new Date());
               courseType.setUpdateTime(new Date());
               courseTypeService.insert(courseType);
           }else {
               courseType.setUpdateTime(new Date());
               courseTypeService.update(courseType);
           }
           return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 删   delete/{id}
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            courseTypeService.delete(id);
            return  AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 查全部 get
    @GetMapping
    @ApiOperation(value = "查询全部")
    public AjaxResult selectAll( ){
        try {
            List<CourseType> list = courseTypeService.selectAll();
            return  AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 查一个 get/{id}
    @GetMapping("/{id}")
    @ApiOperation(value = "查询指定课程类型")
    public AjaxResult selectById(@PathVariable("id") Long id){
        try {
            CourseType courseType = courseTypeService.selectById(id);
            return  AjaxResult.me().setResultObj(courseType);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }

    // 批量删除 @PatchMapping
    @PatchMapping
    @ApiOperation(value = "批量删除")
    public AjaxResult batchDelete(@RequestBody List<Long>  ids){
        try {
            courseTypeService.batchDelete(ids);
            return  AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }

    // 高级查询+分页 @PostMapping
    @PostMapping
    @ApiOperation(value = "高级查询+分页")
    public AjaxResult queryPage(@RequestBody CourseTypeQuery courseTypeQuery){
        try {
            PageInfo<CourseType> pageInfo = courseTypeService.queryPage(courseTypeQuery);
            return  AjaxResult.me().setResultObj(pageInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }


    // 查全部 get
    @GetMapping("/tree")
    @ApiOperation(value = "查询课程类型树")
    public AjaxResult getTree( ){
        try {
            List<CourseType> all = courseTypeService.selectAll();
            List<CourseType> list = courseTypeService.getTree(all);
            return  AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("查询课程类型树失败");
        }
    }
}
