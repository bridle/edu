package org.ma.course.service;


import org.ma.basic.service.IBaseService;
import org.ma.course.domain.Teacher;

/**
 * <p>
 * 老师表 服务类
 * </p>
 *
 * @author xiong
 * @since 2024-08-23
 */
public interface ITeacherService extends IBaseService<Teacher> {

}
