package org.ma.course.service;


import org.ma.basic.service.IBaseService;
import org.ma.basic.util.AjaxResult;
import org.ma.course.domain.Course;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiong
 * @since 2024-08-23
 */
public interface ICourseService extends IBaseService<Course> {
    AjaxResult upload(MultipartFile file);

    AjaxResult onsale(List<Long> ids);

    AjaxResult offsale(List<Long> ids);
}
