package org.ma.course.service.impl;

import org.ma.basic.service.impl.BaseServiceImpl;
import org.ma.course.domain.CourseDetail;
import org.ma.course.service.ICourseDetailService;
import org.springframework.stereotype.Service;

@Service
public class CourseDetailServiceImpl extends BaseServiceImpl<CourseDetail> implements ICourseDetailService {
}
