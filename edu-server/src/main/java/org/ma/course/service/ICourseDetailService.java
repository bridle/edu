package org.ma.course.service;


import org.ma.basic.service.IBaseService;
import org.ma.course.domain.CourseDetail;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiong
 * @since 2024-08-28
 */
public interface ICourseDetailService extends IBaseService<CourseDetail> {

}
