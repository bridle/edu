package org.ma.course.service;


import org.ma.basic.service.IBaseService;
import org.ma.course.domain.CourseType;

import java.util.List;

public interface ICourseTypeService extends IBaseService<CourseType> {


    List<CourseType> getTree(List<CourseType> all );
}


