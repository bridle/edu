package org.ma.course.service.impl;

import org.ma.basic.service.impl.BaseServiceImpl;
import org.ma.course.domain.Teacher;
import org.ma.course.service.ITeacherService;
import org.springframework.stereotype.Service;

/**
 * 业务实现类：老师表
 */
@Service
public class TeacherServiceImpl extends BaseServiceImpl<Teacher> implements ITeacherService {

}
