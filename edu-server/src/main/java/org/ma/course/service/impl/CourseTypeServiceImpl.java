package org.ma.course.service.impl;

import org.ma.basic.service.impl.BaseServiceImpl;
import org.ma.course.domain.CourseType;
import org.ma.course.mapper.CourseTypeMapper;
import org.ma.course.service.ICourseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
@Service
@CacheConfig(cacheNames = "courseType")
public class CourseTypeServiceImpl extends BaseServiceImpl<CourseType> implements ICourseTypeService {
    @Autowired
    private CourseTypeMapper courseTypeMapper;

    @Transactional
    @Override
    public void insert(CourseType courseType) {
        courseTypeMapper.insert(courseType);
        // 处理dir_path
        // 判断 courseType.parentId 是否存在
        if (Objects.isNull(courseType.getParent().getId())){
            // 没有父级 路径就是自己的id
            courseType.setDirPath("/"+courseType.getId());
        }else{
            // 有父级先查父级 然后父级的路径+自己id
            CourseType parent = courseType.getParent();
            CourseType courseType1 = courseTypeMapper.selectById(parent.getId());
            courseType.setDirPath(courseType1.getDirPath()+"/"+courseType.getId());
        }
        courseTypeMapper.update(courseType);

    }

    @Transactional
    @Override
    public void update(CourseType courseType) {
        // 处理dir_path
        if (Objects.isNull(courseType.getParent().getId())){
            courseType.setDirPath("/"+courseType.getId());
        }else{
            CourseType courseType1 = courseTypeMapper.selectById(courseType.getParent().getId());
            courseType.setDirPath(courseType1.getDirPath()+"/"+courseType.getId());
        }
        courseTypeMapper.update(courseType);
    }

    /**
     * SpringCache 是使用aop来完成的 只有你使用javabean调方法 才会有aop的操作
     * @return
     */
    @Override
    public List<CourseType> getTree(List<CourseType> all ) {
        // 我们需要查询的是 一级树 包含 二级树  甚至包含三级树.... n级树
        // 我定义一个
        List<CourseType> listTree = new ArrayList<>();
        // 所有的部门都查询出来 all
//        List<CourseType> all = courseTypeMapper.selectAll();
        // 把查出来的 all 转成一个 map<id,CourseType>
        Map<Long, CourseType> map =
                all.stream().collect(Collectors.toMap(CourseType::getId, x -> x));
        // 遍历list
        for (CourseType courseType:all ) {
            // value判断 value.getParent 是不是null
            if (Objects.isNull(courseType.getParent())){
                // 如果 Parent 是null  那么就是加入 listTree
                listTree.add(courseType);
            } else{
                // 如果 Parent 不是null 我就获取他的   getChildren.add(value)
                map.get(courseType.getParent().getId()).getChildren().add(courseType);
            }
        }
        return listTree;
    }
}
