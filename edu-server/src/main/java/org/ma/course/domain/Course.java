package org.ma.course.domain;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import org.ma.basic.domain.BaseDomain;

/**
 * 实体类：
 */
@Data
@AllArgsConstructor  //全参构造
@NoArgsConstructor //无参构造
public class Course extends BaseDomain {
    private Long id;
    @ApiModelProperty("课程名称")
    private String name;
    @ApiModelProperty("适用人群")
    private String forUser;
    @ApiModelProperty("课程分类")
    private Long courseTypeId;
    private String gradeName;
    @ApiModelProperty("课程等级")
    private Long gradeId;
    @ApiModelProperty("课程状态，下线：0 ， 上线：1")
    private Integer status;
    @ApiModelProperty("添加课程的后台用户的ID")
    private Long loginId;
    @ApiModelProperty("添加课程的后台用户")
    private String loginUserName;
    @ApiModelProperty("课程的开课时间")
    private Date startTime;
    @ApiModelProperty("课程的节课时间")
    private Date endTime;
    @ApiModelProperty("封面，云存储地址")
    private String pic;
    @ApiModelProperty("时长，以分钟为单位")
    private Integer totalMinute;
    @ApiModelProperty("上线时间")
    private Date onlineTime;
    @ApiModelProperty("章节数")
    private Integer chapterCount;
    @ApiModelProperty("讲师，逗号分隔多个")
    private String teacherNames;
    @ApiModelProperty("内容")
    private String intro;
}
