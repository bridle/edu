package org.ma.course.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import org.ma.basic.domain.BaseDomain;

/**
 * 实体类：老师表
 */
@Data
@AllArgsConstructor  //全参构造
@NoArgsConstructor //无参构造
public class Teacher extends BaseDomain {

    private Long id;
    private String name;
    @ApiModelProperty("简介")
    private String intro;
    @ApiModelProperty("技术栈")
    private String technology;
    @ApiModelProperty("职位，高级讲师，架构师")
    private String position;
    @ApiModelProperty("头像")
    private String headImg;
    private String tags;
}
