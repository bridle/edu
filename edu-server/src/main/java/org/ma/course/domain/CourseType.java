package org.ma.course.domain;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ma.basic.domain.BaseDomain;

/**
* 课程目录
* @TableName t_course_type
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseType extends BaseDomain {

    /**
    * 主键ID
    */
    @ApiModelProperty("主键ID")
    private Long id;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 修改时间
    */
    @ApiModelProperty("修改时间")
    private Date updateTime;
    /**
    * 类型名
    */
    @ApiModelProperty("类型名")
    private String name;
    /**
    * 父ID
    */
    @ApiModelProperty("父ID")
    private Long parentId;
    @ApiModelProperty("父对象")
    private CourseType parent;
    /**
    * 图标
    */
    @ApiModelProperty("图标")
    private String logo;
    /**
    * 描述
    */
    @ApiModelProperty("描述")
    private String description;
    /**
    * 索引排序
    */
    @ApiModelProperty("索引排序")
    private Integer sortIndex;
    /**
    * 课程类型路径
    */
    @ApiModelProperty("课程类型路径")
    private String dirPath;
    /**
    * 课程数量
    */
    @ApiModelProperty("课程数量")
    private Integer totalCount;

    @ApiModelProperty("课程子对象")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<CourseType> children = new ArrayList<>();


}
