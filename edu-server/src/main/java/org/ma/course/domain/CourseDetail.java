package org.ma.course.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import org.ma.basic.domain.BaseDomain;

/**
 * 实体类：
 */
@Data
@AllArgsConstructor  //全参构造
@NoArgsConstructor //无参构造
public class CourseDetail extends BaseDomain {

    private Long id;
    @ApiModelProperty("详情")
    private String description;
    @ApiModelProperty("简介")
    private String intro;
}
