package org.ma.org.service.impl;

import org.ma.basic.service.impl.BaseServiceImpl;
import org.ma.org.domain.Employee;
import org.ma.org.service.IEmployeeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService {
}

