package org.ma.org.service;

import org.ma.basic.service.IBaseService;
import org.ma.org.domain.Department;
import java.util.List;

public interface IDepartmentService extends IBaseService<Department> {
    List<Department> getTree();
}
