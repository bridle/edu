package org.ma.org.mapper;

import org.ma.basic.mapper.BaseMapper;
import org.ma.org.domain.Department;

public interface DepartmentMapper extends BaseMapper<Department> {

}
