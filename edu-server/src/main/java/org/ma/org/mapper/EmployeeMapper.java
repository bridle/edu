package org.ma.org.mapper;

import org.ma.basic.mapper.BaseMapper;
import org.ma.org.domain.Employee;

public interface EmployeeMapper extends BaseMapper<Employee> {

}
