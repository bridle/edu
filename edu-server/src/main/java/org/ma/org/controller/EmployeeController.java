package org.ma.org.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.org.domain.Employee;
import org.ma.org.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/employee")
@Api(tags = "员工管理")
public class EmployeeController {
    @Autowired
    private IEmployeeService employeeService;
    // 增/ 改 put 和 post请求 拿值是从 body里面取的
    @PutMapping
    @ApiOperation(value = "新增/修改")
    public AjaxResult save(@RequestBody Employee employee){
        try {
            if (Objects.isNull(employee.getId()))employeeService.insert(employee);
            else employeeService.update(employee);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 删   delete/{id}
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            employeeService.delete(id);
            return  AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 查全部 get
    @GetMapping
    @ApiOperation(value = "查询全部")
    public AjaxResult selectAll( ){
        try {
            List<Employee> list = employeeService.selectAll();
            return  AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 查一个 get/{id}
    @GetMapping("/{id}")
    @ApiOperation(value = "查询指定部门")
    public AjaxResult selectById(@PathVariable("id") Long id){
        try {
            Employee employee = employeeService.selectById(id);
            return  AjaxResult.me().setResultObj(employee);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
}
