package org.ma.org.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.ma.basic.util.AjaxResult;
import org.ma.basic.util.PageInfo;
import org.ma.org.domain.Department;
import org.ma.org.query.DepartmentQuery;
import org.ma.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/department")
@Api(tags = "部门管理")
public class DepartmentController {
    @Autowired
    private IDepartmentService departmentService;
    // 增/ 改 put 和 post请求 拿值是从 body里面取的
    @PutMapping
    @ApiOperation(value = "新增/修改")
    public AjaxResult save(@RequestBody Department department){
        try {
            if (Objects.isNull(department.getId())){
                department.setCreateTime(new Date());
                department.setUpdateTime(new Date());
                departmentService.insert(department);
            }else {
                department.setUpdateTime(new Date());
                departmentService.update(department);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 删   delete/{id}
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            departmentService.delete(id);
            return  AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 查全部 get
    @GetMapping
    @ApiOperation(value = "查询全部")
    public AjaxResult selectAll( ){
        try {
            List<Department> list = departmentService.selectAll();
            return  AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }
    // 查一个 get/{id}
    @GetMapping("/{id}")
    @ApiOperation(value = "查询指定部门")
    public AjaxResult selectById(@PathVariable("id") Long id){
        try {
            Department department = departmentService.selectById(id);
            return  AjaxResult.me().setResultObj(department);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }

    // 批量删除 @PatchMapping
    @PatchMapping
    @ApiOperation(value = "批量删除")
    public AjaxResult batchDelete(@RequestBody List<Long>  ids){
        try {
            departmentService.batchDelete(ids);
            return  AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }

    // 高级查询+分页 @PostMapping
    @PostMapping
    @ApiOperation(value = "高级查询+分页")
    public AjaxResult queryPage(@RequestBody DepartmentQuery departmentQuery){
        try {
            PageInfo<Department> pageInfo = departmentService.queryPage(departmentQuery);
            return  AjaxResult.me().setResultObj(pageInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("操作失败");
        }
    }


    // 查全部 get
    @GetMapping("/tree")
    @ApiOperation(value = "查询部门树")
    public AjaxResult getTree( ){
        try {
            List<Department> list = departmentService.getTree();
            return  AjaxResult.me().setResultObj(list);
        } catch (Exception e) {
            e.printStackTrace();
            return  AjaxResult.me().setSuccess(false).setMessage("查询部门树失败");
        }
    }
}


