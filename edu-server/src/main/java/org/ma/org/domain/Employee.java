package org.ma.org.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ma.basic.domain.BaseDomain;


/**
 *
 * @TableName t_employee
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends BaseDomain {

    /**
     * 员工ID
     */
    @ApiModelProperty("员工ID")
    private Long id;
    /**
     * 员工名称
     */
    @ApiModelProperty("员工名称")
    private String username;
    /**
     * 员工电话
     */
    @ApiModelProperty("员工电话")
    private String phone;
    /**
     * 员工邮箱
     */
    @ApiModelProperty("员工邮箱")
    private String email;
    /**
     * 盐值
     */
    @ApiModelProperty("盐值")
    private String salt;
    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;
    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;
    /**
     * 员工状态 - 1启用，0禁用
     */
    @ApiModelProperty("员工状态 - 1启用，0禁用")
    private Integer state;
    /**
     * 所属部门ID
     */
    @ApiModelProperty("所属部门ID")
    private Long departmentId;
    /**
     * 登录信息ID
     */
    @ApiModelProperty("登录信息ID")
    private Long logininfoId;
    /**
     * 店铺ID
     */
    @ApiModelProperty("店铺ID")
    private Long shopId;


}
