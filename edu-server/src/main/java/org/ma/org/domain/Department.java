package org.ma.org.domain;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ma.basic.domain.BaseDomain;

/**
 *
 * @TableName t_department
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department extends BaseDomain {
    private Long id;
    /**
     * 部门编号
     */
    private String sn;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "部门名称")
    private String name;
    /**
     * 部门的上级分类层级id
     */
    private String dirPath;
    /**
     * 部门状态 - 1启用，0禁用
     */
    private Integer state;
    /**
     * 部门管理员，关联Employee表id
     */
    private Long managerId;
    private Employee manager;
    /**
     * 上级部门
     */
    private Long parentId;
    private Department parent;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 子集元素
     */
    // 改注解是用于解决 子集为null的时候 前端会显示一个空页面
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Department> children = new ArrayList<>();


}
