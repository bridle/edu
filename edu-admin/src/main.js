import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
//import './assets/theme/theme-green/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
// import Mock from './mock'
// Mock.bootstrap();
import 'font-awesome/css/font-awesome.min.css'
import axios from 'axios'
//配置axios的全局基本路径 所有的axios的请求都会加上一个前缀 叫做http://localhost:8080/
// axios.defaults.baseURL='/api'
axios.defaults.baseURL='http://localhost:8080'
//全局属性配置，在任意组件内可以使用this.$http获取axios对象
Vue.prototype.$http = axios
Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
    routes
})
// axios 的拦截器 -> 整个axios在发请求之前都会被拦截下来
// axios前置拦截
axios.interceptors.request.use(
    config => {
        let token = sessionStorage.getItem("token");
        config.headers['token'] = token;
        return config
    },
    error=>{}
)
// axios后置拦截
axios.interceptors.response.use(
    response=>{
        console.log("axios的response",response)
        let data = response.data
        console.log("axios的success",data.success)
        console.log("axios的message",data.message)
        if(!data.success && data.message == 'noLogin'){
            // this.$router.push({ path: '/login' });
            router.push({ path: '/login' });
        }
        return response
    },
    error => {
        Promise.reject(error)
    }
)
// 路由拦截
router.beforeEach((to, from, next) => {
    //NProgress.start();
    if (to.path == '/login') {
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('logininfo');
    }
    let user = JSON.parse(sessionStorage.getItem('logininfo'));
    if (!user && to.path != '/login') {
        next({ path: '/login' })
    } else {
        next()
    }
})

//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
    //el: '#app',
    //template: '<App/>',
    router,
    store,
    //components: { App }
    render: h => h(App)
}).$mount('#app')

