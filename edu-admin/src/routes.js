import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Table from './views/nav1/Table.vue'
import echarts from './views/charts/echarts.vue'
//========================导入自己组件================================
import department from "./views/org/Department";
import courseType from "./views/course/CourseType";
import course from "./views/course/course";

let routes = [
    {
        path: '/login',  //地址
        component: Login,  //绑定的组件
        name: '',  //名称，由于不显示，不需要名字
        hidden: true //当前路由在页面隐藏不显示，跳转后展示组件内容
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    {
        path: '/',
        component: Home,
        name: 'Charts',
        iconCls: 'el-icon-s-home',//修改为elmentUI提供的icon，图标统一
        leaf: true,//只有一个节点
        children: [
            { path: '/echarts', component: echarts, name: 'echarts' }
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '组织结构管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/department', component: department, name: '部门管理' },
            { path: '/employee', component: Table, name: '员工管理' },
            { path: '/shop', component: Table, name: '门店管理' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '课程管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/courseType', component: courseType, name: '课程类型管理' },
            { path: '/course', component: course, name: '课程管理' },
        ]
    },
    /*{
          path: '/',
          component: Home,
          name: '导航二',
          iconCls: 'fa fa-id-card-o',
          children: [
              { path: '/page4', component: Page4, name: '页面4' },
              { path: '/page5', component: Page5, name: '页面5' }
          ]
      },
      {
          path: '/',
          component: Home,
          name: '',
          iconCls: 'fa fa-address-card',
          leaf: true,//只有一个节点
          children: [
              { path: '/page6', component: Page6, name: '导航三' }
          ]
      }*/
];

export default routes;